﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    private float health = 1.0f;

    public void RecieveDamage(float a_damage)
    {
        health -= a_damage;
    }

    virtual public void OnTriggerEnter2D(Collider2D a_collision)
    {
		// logic for projectile collisions should be moved into a projectile script
        if (a_collision.gameObject.transform.root.GetComponentInChildren<ProjectileBase>() != null)
        {
            ProjectileBase proj = a_collision.transform.root.GetComponentInChildren<ProjectileBase>();
            RecieveDamage(proj.damage);

            Destroy(proj.transform.root.gameObject);
        }
    }
}
