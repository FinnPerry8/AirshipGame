﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
	public float maxMoveDelta;
	public float collisionRayLength;

	public float weightToTarget;
	public float weightAwayFromTerrain;

	private const int DIRECTION_COUNT = 16;
	public TransformScriptableObject target;
	private Rigidbody2D rb;

	private void Start()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate()
	{
		// direction weights
		float[] weights = new float[DIRECTION_COUNT];
		for (int i = 0; i < DIRECTION_COUNT; ++i)
		{
			weights[i] = 0.0f;
		}

		// weigh towards the target
		Vector2 toTarget = target.Value.position - transform.position;
		for (int i = 0; i < DIRECTION_COUNT; ++i)
		{
			Vector2 dir = GetDir(i);
			float dot = Vector2.Dot(dir, toTarget);
			weights[i] += dot * weightToTarget;
		}

		// weight away from terrain
		for (int i = 0; i < DIRECTION_COUNT; ++i)
		{
			Vector2 dir = GetDir(i);
			var hit = Physics2D.Raycast(transform.position, dir, collisionRayLength, LayerMask.GetMask(new string[]{"Terrain"}));
			if (hit.transform != null)
			{
				weights[i] -= weightAwayFromTerrain;
			}
		}

		// sigmoid the weights
		for (int i = 0; i < DIRECTION_COUNT; ++i)
		{
			weights[i] = Helper.Sigmoid(weights[i]);
		}

		// move
		Vector2 move = Vector2.zero;
		for (int i = 0; i < DIRECTION_COUNT; ++i)
		{
			move += GetDir(i) * weights[i];
		}
		rb.velocity = Vector3.MoveTowards(rb.velocity, move, maxMoveDelta * Time.fixedDeltaTime);

		// display weight debug lines
		for (int i = 0; i < DIRECTION_COUNT; ++i)
		{
			Vector2 dir = GetDir(i);
			Color c = (weights[i] == 0.5f) ? Color.black : ((weights[i] > 0.5f) ? Color.blue : Color.red);
			float length = Mathf.Abs(Mathf.Lerp(-1.0f, 1.0f, weights[i]));
			dir *= length + 1.0f;
			Debug.DrawLine(transform.position, transform.position + (Vector3)dir, c, Time.fixedDeltaTime);
		}
	}

	private Vector2 GetDir(int a_index)
	{
		float angle = (360.0f / (float)DIRECTION_COUNT) * (float)a_index;
		angle = Mathf.Deg2Rad * angle;
		Vector2 dir = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
		return dir;
	}
}
