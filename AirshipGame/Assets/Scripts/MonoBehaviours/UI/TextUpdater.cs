﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextUpdater : MonoBehaviour
{
	public StringScriptableObject value;
	private UnityEngine.UI.Text text;

	private void Awake()
	{
		text = GetComponent<UnityEngine.UI.Text>();
		value.OnSet.AddListener(UpdateText);
	}

	private void UpdateText()
	{
		text.text = value.Value;
	}
}
