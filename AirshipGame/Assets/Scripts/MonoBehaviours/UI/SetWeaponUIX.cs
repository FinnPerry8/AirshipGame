﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWeaponUIX : MonoBehaviour
{
    public Transform pseudoRoot;
    private void Update()
    {
        transform.position = new Vector3(pseudoRoot.position.x, pseudoRoot.position.y, transform.position.z);
    }
}
