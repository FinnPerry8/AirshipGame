﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopMenu : MonoBehaviour
{
	public GameObjectScriptableObject shopMenu;

	private void Awake()
	{
		shopMenu.Value = gameObject;

		// set the button click event
		List<Button> buttonlist = new List<Button>();
		buttonlist.AddRange(GetComponentsInChildren<Button>(true));

		LevelExit town = GameObject.FindObjectOfType<LevelExit>();
		buttonlist[0].onClick.AddListener(town.Continue);

		PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		buttonlist[1].onClick.AddListener(player.FuelUP);

		buttonlist[2].onClick.AddListener(player.AmmoFill);

		buttonlist[3].onClick.AddListener(player.FreshMeat);
	}
}
