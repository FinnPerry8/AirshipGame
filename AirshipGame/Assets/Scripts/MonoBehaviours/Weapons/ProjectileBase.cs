﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBase : MonoBehaviour
{
    private float despawnTimer = 10.0f;
    public float damage = 1.0f;
    public float speed = 3.0f;

    void Update()
    {
        transform.Translate((transform.right * speed) * Time.deltaTime, Space.World);

        if (despawnTimer <= 0.0f)
        {
            Destroy(this.gameObject);
        }
        else
        {
            despawnTimer -= Time.deltaTime;
        }
    }
}
