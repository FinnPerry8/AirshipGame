﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBase : MonoBehaviour
{
	/*
    enum ProjectTypes{
        Physical,Thermal,Explosive
    }
	*/
    public float baseCooldown = 1.0f;
    public uint requiredCrew = 3;
    public Transform target;
    private GameObject weaponRoot;
    public float maxAngle = 0.3f;
    public float rotSpeed;

    private float actualCooldown;
    private bool loaded = false;
    private uint numOfCrew = 3;
    // private ProjectTypes projectileType = ProjectTypes.Physical;
    private float range = 5.0f;
    private JobBase myJob;
    private PlayerController PlayerCont;

	private void Awake()
	{
		weaponRoot = transform.parent.gameObject;
	}

    private void Start()
    {
        actualCooldown = baseCooldown;
        if (GetComponent<JobBase>() != null)
        {
            myJob = GetComponent<JobBase>();
            myJob.requiredNumOfCrew = requiredCrew;
            PlayerCont = transform.root.GetComponent<PlayerController>();
        }
    }

    private void Update()
    {
        if (!loaded)
        {
            if (actualCooldown <= 0.0f)
            {
                loaded = true;
            }
            else
            {
                actualCooldown -= Time.deltaTime;
            }
        }

        // currently the weapon will only rotate and fire when the target is within range, maybe consider always rotating and only fire when within range
		if (target != null)
		{
			if (range > (transform.position - target.position).magnitude)
			{
				if (Vector2.Dot(weaponRoot.transform.right, target.position -  transform.position) > 0.3f)
				{
					Vector3 targetRight = target.position - transform.position;
					transform.right = Vector3.MoveTowards(transform.right, targetRight, rotSpeed * Time.deltaTime);
					
					RaycastHit2D rayhit2D = Physics2D.Raycast(gameObject.transform.position, target.position - transform.position);

					if (rayhit2D.collider.gameObject.transform.root.gameObject == target && loaded)
					{
						if (myJob != null)
						{
							if (myJob.currentNumOfCrew > 0 && PlayerCont.ammo > 0)
							{
								PlayerCont.ammo--;
								PlayerCont.ResetUI();
								FireWeapon();
							}
						}
						else
						{
							FireWeapon();
						}
					}
				}
			}
		}
    }

    private void FireWeapon()
    {
		int current = myJob == null ? (int)numOfCrew : myJob.currentNumOfCrew;
		int total = myJob == null ? (int)numOfCrew : (int)myJob.requiredNumOfCrew;
		actualCooldown = baseCooldown * (1.0f - Helper.JobEffiency(current, total));

        GameObject.Instantiate(Resources.Load("Prefabs/Projectile"), transform.position + (transform.right * 0.5f), gameObject.transform.rotation);
        loaded = false;
    }
}
