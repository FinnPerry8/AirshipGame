﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralTerrain : MonoBehaviour
{
	// map data
	class Map
	{
		public int[,] map;
		public int sections;
	};

	// width/height of the terrain in world space
	[SerializeField]
	private float size;

	// pixels on the terrain map per unit of world space
	[SerializeField]
	private float pixelsPerUnit;

	// pixels on the noise map per unit of world space
	[SerializeField]
	private float noisePixelsPerUnit;

	// noise values lower than the thc_reshhold are culled
	[SerializeField]
	private float noiseThresh;

	// constant values calculated at start
	private int RESOLUTION;
	private float NOISE_SIZE;

	// object references
	private SpriteRenderer sprite;

	private void Awake()
	{
		sprite = GetComponent<SpriteRenderer>();
	}

	private void Start()
	{
		// set constants
		RESOLUTION = (int)(size * pixelsPerUnit);
		NOISE_SIZE = size * noisePixelsPerUnit;

		// generate the map
		Map map = CreateMap();

		// create the texture
		Texture2D tex = CreateTexture(map);

		// set the sprite
		sprite.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, RESOLUTION, RESOLUTION), Vector2.one / 2.0f, pixelsPerUnit);

		// create colliders
		CreateBoxColliders(map);
	}

	// generates the map
	// returns an int array of size (c_res * c_res)
	private Map CreateMap()
	{
		// create the int array
		int[,] arr = new int[RESOLUTION, RESOLUTION];

		// generate noise
		const float noiseRange = 1000.0f;
		Vector2 noiseOff = new Vector2(Random.Range(-noiseRange, noiseRange), Random.Range(-noiseRange, noiseRange));
		for (int x = 0; x < RESOLUTION; ++x)
		{
			float posX = Mathf.InverseLerp(0, RESOLUTION - 1, x);
			float noisePosX = Mathf.Lerp(0.0f, NOISE_SIZE, posX);
			for (int y = 0; y < RESOLUTION; ++y)
			{
				float posY = Mathf.InverseLerp(0, RESOLUTION - 1, y);
				float noisePosY = Mathf.Lerp(0.0f, NOISE_SIZE, posY);

				float noise = Mathf.PerlinNoise(noisePosX + noiseOff.x, noisePosY + noiseOff.y);
				arr[x, y] = (noise >= noiseThresh) ? -1 : 0;
			}
		}

		// split into sections
		int sections = CreateMapSections(arr);

		// create the map
		Map map = new Map();
		map.map = arr;
		map.sections = sections;
		return map;
	}

	// splits the map into sections
	// returns the number of sections
	private int CreateMapSections(int[,] a_map)
	{
		// section index
		int section = 1;

		// find an unprocessed section
		for (int x = 0; x < RESOLUTION; ++x)
		{
			for (int y = 0; y < RESOLUTION; ++y)
			{
				if (a_map[x, y] == -1)
				{
					CreateMapSectionsRec(a_map, x, y, section);
					++section;
				}
			}
		}

		return section - 1;
	}

	// recursively set map section values
	private void CreateMapSectionsRec(int[,] a_map, int a_x, int a_y, int a_section)
	{
		if (a_map[a_x, a_y] == -1)
		{
			a_map[a_x, a_y] = a_section;

			// left
			if (a_x > 0)
			{
				CreateMapSectionsRec(a_map, a_x - 1, a_y, a_section);
			}
			// right
			if (a_x < RESOLUTION - 1)
			{
				CreateMapSectionsRec(a_map, a_x + 1, a_y, a_section);
			}
			// bottom
			if (a_y > 0)
			{
				CreateMapSectionsRec(a_map, a_x, a_y - 1, a_section);
			}
			// top
			if (a_y < RESOLUTION - 1)
			{
				CreateMapSectionsRec(a_map, a_x, a_y + 1, a_section);
			}
		}
	}

	// create a texture from a map
	private Texture2D CreateTexture(Map a_map)
	{
		// create random colors
		Color[] cols = new Color[a_map.sections + 1];
		cols[0] = Color.clear;
		for (int i = 1; i < a_map.sections + 1; ++i)
		{
			cols[i] = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f);
		}

		// create the texture
		Texture2D tex = new Texture2D(RESOLUTION, RESOLUTION);
		tex.filterMode = FilterMode.Point;
		tex.wrapMode = TextureWrapMode.Clamp;
		for (int x = 0; x < RESOLUTION; ++x)
		{
			for (int y = 0; y < RESOLUTION; ++y)
			{
				tex.SetPixel(x, y, cols[a_map.map[x, y]]);
			}
		}
		tex.Apply();

		return tex;
	}

	// check if x,y values are within range of the map resolution
	private bool IsIsRange(int a_x, int a_y)
	{
		return a_x >= 0 && a_x < RESOLUTION && a_y >= 0 && a_y < RESOLUTION;
	}

	// get the world position of a map coord
	private Vector2 MapCoordToLocalPos(int a_x, int a_y)
	{
		// get bottom left position
		Vector2 pos = Vector2.one * -(size / 2.0f);

		// coord offset
		float unitSize = 1.0f / pixelsPerUnit;
		pos += new Vector2(a_x * unitSize, a_y * unitSize);

		// offset so that the position is in the center
		pos += Vector2.one * (unitSize / 2.0f);

		return pos;
	}
	
	// create box colliders on terrain edges
	private void CreateBoxColliders(Map a_map)
	{
		// get the size of 1 box
		float boxSize = 1.0f / pixelsPerUnit;

		for (int x = 0; x < RESOLUTION; ++x)
		{
			for (int y = 0; y < RESOLUTION; ++y)
			{
				// get the current section
				int section = a_map.map[x, y];
				if (section != 0)
				{
					// check if the surrounding area is empty
					bool leftEmpty = IsIsRange(x - 1, y) ? (a_map.map[x - 1, y] != section) : true;
					bool rightEmpty = IsIsRange(x + 1, y) ? (a_map.map[x + 1, y] != section) : true;
					bool downEmpty = IsIsRange(x, y - 1) ? (a_map.map[x, y - 1] != section) : true;
					bool upEmpty = IsIsRange(x, y + 1) ? (a_map.map[x, y + 1] != section) : true;

					// create a box collider
					if (leftEmpty || rightEmpty || downEmpty || upEmpty)
					{
						BoxCollider2D box = gameObject.AddComponent<BoxCollider2D>();
						box.size = Vector2.one * boxSize;
						box.offset = MapCoordToLocalPos(x, y);
					}
				}
			}
		}
	}

	// convert a map to string
	private string MapToString(Map a_map)
	{
		string str = "";

		for (int y = 0; y < RESOLUTION; ++y)
		{
			for (int x = 0; x < RESOLUTION; ++x)
			{
				int Y = RESOLUTION - 1 - y;
				if (a_map.map[x, Y] == 0)
				{
					str += "  ";
				}
				else
				{
					string s = a_map.map[x, Y].ToString();
					str += s;
					str += ",";
				}
			}
			str += "\n";
		}

		return str;
	}
}
