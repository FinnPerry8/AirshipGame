﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
	// objects to enable
	[SerializeField] private GameObject door;
	public GameObjectScriptableObject shopMenu;

	private void OnTriggerEnter2D(Collider2D a_col)
	{
		if (a_col.transform.root.GetComponent<PlayerController>())
		{
			// enable objects
			door.SetActive(true);
			shopMenu.Value.SetActive(true);
		}
	}

	public void Continue()
	{
		DontDestroyOnLoad(GameObject.FindGameObjectWithTag("Player"));
		SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}
}
