﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper
{
	public static T[] GetCompontsWithTag<T>(string a_tag)
	{
		List<T> components = new List<T>();
		foreach (GameObject g in GameObject.FindGameObjectsWithTag(a_tag))
		{
			T c = g.GetComponent<T>();
			if (c != null)
			{
				components.Add(c);
			}
		}
		return components.ToArray();
	}

	public static float Sigmoid(float a_value)
	{
		return 1.0f / (1.0f + Mathf.Exp(-a_value));
	}

	private static AnimationCurve JobEfficiencyCurve()
	{
		AnimationCurve curve = new AnimationCurve();
		curve.AddKey(0.0f, 0.0f);
		curve.AddKey(0.5f, 1.0f);
		curve.AddKey(1.0f, 0.0f);
		return curve;
	}

	public static float JobEffiency(int a_crewCount, int a_targetCrewCount)
	{
		if (a_crewCount <= 0)
		{
			return 0.0f;
		}
		AnimationCurve curve = JobEfficiencyCurve();
		int num = a_crewCount - a_targetCrewCount;
		float efficiency = Sigmoid((float)num);
		return curve.Evaluate(efficiency);
	}
}