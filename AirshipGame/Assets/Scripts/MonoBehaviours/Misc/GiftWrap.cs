﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftWrap : MonoBehaviour
{
    public List<Vector2> points;

    private void Update()
    {
        // get points
        List<Vector2> tempPoints = new List<Vector2>();
        for (int i = 0; i < transform.childCount; ++i)
        {
            tempPoints.Add(transform.GetChild(i).localPosition);
        }
        List<int> indices = GenerateGiftWrap(tempPoints);
        points = new List<Vector2>();
        for (int i = 0; i < indices.Count; ++i)
        {
            points.Add(tempPoints[indices[i]]);
        }

        // display path
        for (int i = 0; i < points.Count; ++i)
        {
            Vector3 a = transform.position + new Vector3(points[i].x, points[i].y, 0.0f);
            int j = (i == points.Count - 1) ? 0 : (i + 1);
            Vector3 b = transform.position + new Vector3(points[j].x, points[j].y, 0.0f);
            Debug.DrawLine(a, b, Color.red);
        }
    }

    private List<int> GenerateGiftWrap(List<Vector2> a_points)
    {
        List<int> indices = new List<int>();

        // get left most point
        int leftMost = 0;
        for (int i = 1; i < a_points.Count; ++i)
        {
            if (a_points[i].x < a_points[leftMost].x)
            {
                leftMost = i;
            }
        }
        indices.Add(leftMost);

        // wrap
        Vector2 prevCurrentToLast = Vector2.down;
        for (int dontInfiniteLoop = 0; dontInfiniteLoop < 1000; ++dontInfiniteLoop)
        {
            // find point with lowest angle
            int last = indices[indices.Count - 1];
            int lowest = -1;
            float lowestAngle = 0.0f;
            Vector2 lowestCurrentToLast = Vector2.zero;
            for (int i = 0; i < a_points.Count; ++i)
            {
                // don't check angle against self
                if (i == last)
                {
                    continue;
                }

                // calculate angle
                {
                    Vector2 lastToCurrent = a_points[i] - a_points[last];
                    float angle = -Vector2.SignedAngle(prevCurrentToLast, lastToCurrent);
                    if (angle == 0.0f)
                    {
                        continue;
                    }
                    if (angle < 0.0f)
                    {
                        angle = 360.0f + angle;
                    }
                    if (lowest == -1 || angle < lowestAngle)
                    {
                        lowest = i;
                        lowestAngle = angle;
                        lowestCurrentToLast = -lastToCurrent;
                    }
                }
            }
            prevCurrentToLast = lowestCurrentToLast;

            // stop if the wrap has finished
            if (lowest == leftMost)
            {
                break;
            }

            // add the index
            indices.Add(lowest);
        }

        return indices;
    }
}
