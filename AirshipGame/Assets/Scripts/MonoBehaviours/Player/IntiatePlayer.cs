﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntiatePlayer : MonoBehaviour
{
    private void Awake()
    {
        if (!GameObject.FindGameObjectWithTag("Player"))
        {
            Instantiate(Resources.Load("Prefabs/Player"), transform.position, transform.rotation);
        }
        else
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.transform.position = new Vector3(-55, 0, 0);
            player.GetComponent<PlayerController>().Awake();
            player.GetComponent<PlayerController>().ResetTarget();
        }
    }
}
