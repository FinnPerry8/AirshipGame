﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	// movement speed
	public float rotSpeed;
	public float rotMaxDelta;
	public float moveSpeed;
	public float moveMaxDelta;
	private float movSpeedaffector;

	// gameobject components
	private Rigidbody2D rb;

	// Callum's Shit
	private float health = 1.0f;
	private int oncePerSecTimer = 60;
	private List<WeaponBase> weapons;
	private EnemyBase[] enemies;
	private JobManager jobManager;
	private float fuel = 2000.0f;
	public int ammo = 200;
	public Text fuelUI;
	public Text ammoUI;

	//Job System WorldCanvas Stuff
	public GameObject canvas;
	public Text IdleCrewMemers;

	// inputs
	private float turnInput;
	private float moveInput;

	public TransformScriptableObject playerTransform;

	public void Awake()
	{
		playerTransform.Value = transform;
		rb = GetComponent<Rigidbody2D>();
		weapons = new List<WeaponBase>(GetComponentsInChildren<WeaponBase>());
		jobManager = GetComponent<JobManager>();
		enemies = Helper.GetCompontsWithTag<EnemyBase>("Enemy");
	}

	private void Update()
	{
		// cache keyboard input
		turnInput = -Input.GetAxis("Horizontal");
		moveInput = Input.GetAxis("Vertical");

		if (Input.GetKeyDown(KeyCode.Tab))
		{
			canvas.SetActive(!canvas.activeSelf);
		}

		movSpeedaffector = jobManager.ShipMoveSpeedAffector();
	}

	private void FixedUpdate()
	{
		if (fuel <= 0.0f)
		{
			turnInput = 0;
			moveInput = 0;
		}

		// move angular velocity towards the target
		{
			float targetRotation = turnInput * (rotSpeed * movSpeedaffector);
			rb.angularVelocity = Mathf.MoveTowards(rb.angularVelocity, targetRotation, rotMaxDelta * Time.deltaTime);
			if (turnInput != 0.0f)
			{
				fuel -= 0.05f;
				ResetUI();
			}
		}

		// move velocity towards the target
		{
			Vector2 targetVelocity = (Vector2)(transform.right) * (moveInput * (moveSpeed * movSpeedaffector));
			rb.velocity = Vector2.MoveTowards(rb.velocity, targetVelocity, moveMaxDelta * Time.deltaTime);
			if (moveInput != 0.0f)
			{
				fuel -= 0.05f;
				ResetUI();
			}
		}

		if (oncePerSecTimer == 1)
		{
			ResetTarget();
		}
		else
		{
			oncePerSecTimer -= 1;
		}
	}

	private void OnTriggerEnter2D(Collider2D a_collision)
	{
		if (a_collision.gameObject.transform.root.GetComponentInChildren<ProjectileBase>() != null)
		{
			ProjectileBase proj = a_collision.gameObject.transform.root.GetComponentInChildren<ProjectileBase>();
			RecieveDamage(proj.damage);

			Destroy(proj.transform.root.gameObject);
		}
	}

	private void RecieveDamage(float a_damage)
	{
		health -= a_damage;
	}

	public void ResetUI()
	{
		fuelUI.text = "Fuel: " + fuel.ToString("f2");
		ammoUI.text = "Ammo: " + ammo.ToString();
	}

	public void ResetTarget()
	{
		EnemyBase closestEnemy = enemies[0];
		for (int i = 0; i < enemies.Length; i++)
		{
			if ((enemies[i].transform.position - transform.position).magnitude < (closestEnemy.transform.position - gameObject.transform.position).magnitude)
			{
				closestEnemy = enemies[i];
			}
		}

		for (int i = 0; i < weapons.Count; i++)
		{
			weapons[i].target = closestEnemy.transform;
		}
		oncePerSecTimer = 60;

		//constant fuel tick
		if (fuel < 0.0f)
		{
			fuel -= 1.0f;
			ResetUI();
		}
	}

	public void FuelUP()
	{
		fuel = 2000.0f;
		ResetUI();
	}
	public void AmmoFill()
	{
		ammo = 200;
		ResetUI();
	}
	public void FreshMeat()
	{
		jobManager.AddNewIdleCrew();
	}
}
