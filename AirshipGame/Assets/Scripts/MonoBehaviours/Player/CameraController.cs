﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public float scrollSpeed;
	public float zoomMin;
	public float zoomMax;

	public TransformScriptableObject target;
	private Camera cam;

    private void Awake()
    {
		cam = GetComponent<Camera>();
    }

	private void Update()
	{
		// move position
		transform.position = new Vector3(target.Value.position.x, target.Value.position.y, transform.position.z);

		// scroll zoom
		float scrollInput = Input.GetAxis("Mouse ScrollWheel");
		cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - scrollInput * scrollSpeed, zoomMin, zoomMax);
	}
}
