﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobBase : MonoBehaviour
{
    public enum JobType
    {
        Weapon,
        Engine
    }
    public JobType job;
    public int numOfCrewWanted;
    public int currentNumOfCrew;
    public uint requiredNumOfCrew;
    public StringScriptableObject crewNumbersDisplay;
    private void Start()
    {
        ResetUI();
    }

    public void ChangeNumOfCrewWanted(int a_value)
    {
        if (numOfCrewWanted + a_value > requiredNumOfCrew)
        {
            return;
        }
        numOfCrewWanted += a_value;
        if (numOfCrewWanted < 0)
        {
            numOfCrewWanted = 0;
        }
        ResetUI();
    }

    public void ResetUI()
    {
        crewNumbersDisplay.Value = currentNumOfCrew.ToString() + "/" + numOfCrewWanted.ToString();
    }
}
