﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobManager : MonoBehaviour
{
    private List<JobBase> shipJobs = new List<JobBase>();
    private int idleCrew = 3;

    private void Start()
    {
        shipJobs.AddRange(gameObject.GetComponentsInChildren<JobBase>());
    }
    public void ChangeWantedCrewNum(JobBase a_job)
    {
        int diff = a_job.currentNumOfCrew - a_job.numOfCrewWanted;
        for (int i = 0; i < Mathf.Abs(diff); ++i)
        {
            if (a_job.numOfCrewWanted < 0 || a_job.numOfCrewWanted == a_job.currentNumOfCrew)
            {
                return;
            }
            if (a_job.currentNumOfCrew > a_job.numOfCrewWanted)
            {
                a_job.currentNumOfCrew--;
                a_job.ResetUI();
                idleCrew++;
            }
            else
            {
                if (a_job.numOfCrewWanted > 0)
                {
                    if (idleCrew > 0)
                    {
                        a_job.currentNumOfCrew++;
                        a_job.ResetUI();
                        idleCrew--;
                    }
                }
            }
            LookForJobVacancies();
        }
    }

    private void LookForJobVacancies()
    {
        for (int i = 0; i < shipJobs.Count; i++)
        {
            if (idleCrew == 0)
            {
                return;
            }

            if (shipJobs[i].numOfCrewWanted > shipJobs[i].currentNumOfCrew)
            {
                shipJobs[i].currentNumOfCrew++;
                idleCrew--;
                shipJobs[i].ResetUI();
            }
        }
    }

    public float ShipMoveSpeedAffector()
    {
        int totalrequiredCrew = 0;
        int crewEnlisted = 0;
        for (int i = 0; i < shipJobs.Count; i++)
        {
            if (shipJobs[i].job == JobBase.JobType.Engine)
            {
                totalrequiredCrew += (int)shipJobs[i].requiredNumOfCrew;
                crewEnlisted += shipJobs[i].currentNumOfCrew;
            }
        }
		return Helper.JobEffiency(crewEnlisted, totalrequiredCrew);
    }

    public void AddNewIdleCrew()
    {
        idleCrew++;
        LookForJobVacancies();
    }
}
