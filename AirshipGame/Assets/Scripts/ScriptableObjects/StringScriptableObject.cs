﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "StringScriptableObject", menuName = "ScriptableObjects/StringScriptableObject")]
public class StringScriptableObject : ScriptableObject
{
	public string Value
	{
		get => _value;

		set
		{
			_value = value;
			OnSet.Invoke();
		}
	}

	public UnityEvent OnSet;

	private string _value;
}
