﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "TransformScriptableObject", menuName = "ScriptableObjects/TransformScriptableObject")]
public class TransformScriptableObject : ScriptableObject
{
	public Transform Value
	{
		get => _value;

		set
		{
			_value = value;
			OnSet.Invoke();
		}
	}

	public UnityEvent OnSet;

	private Transform _value;
}
