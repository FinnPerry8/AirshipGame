﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "GameObjectScriptableObject", menuName = "ScriptableObjects/GameObjectScriptableObject")]
public class GameObjectScriptableObject : ScriptableObject
{
	public GameObject Value
	{
		get => _value;

		set
		{
			_value = value;
			OnSet.Invoke();
		}
	}

	public UnityEvent OnSet;

	private GameObject _value;
}
